### Запуск приложения
`mvn jetty:run`

### Swagger UI
Swagger UI доступен по URL: http://localhost:8080/swagger-ui.html.
