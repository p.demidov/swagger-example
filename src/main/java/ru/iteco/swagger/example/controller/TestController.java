package ru.iteco.swagger.example.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @ApiOperation("Тест метода GET")
    @GetMapping("/test")
    public String getTestData() {
        return "GET test data";
    }

    @ApiOperation("Тест метода POST")
    @PostMapping("/test")
    public String postTestData(String data) {
        return "Received POST data: " + data;
    }
}
